import math
import os
import random
import re
import sys

#
# Complete the 'camelcase' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def camelcase(s):
    splite = sum(map(str.isupper, s)) + 1
    return splite
if __name__ == '__main__':

    s = input()

    result = camelcase(s)
    print(result)