from random import randint

def recur_fibo(n):
   if n <= 1:
       return n
   else:
       return(recur_fibo(n-1) + recur_fibo(n-2))

terms = randint(0, 21)

for i in range(terms):
    recur_fibo(i)
