import unittest
import fibo

class  fabtest(unittest.TestCase):
    def test_fab_0_should_be_0(self):
        result = fibo.recur_fibo(0)
        self.assertEqual(result,0)

    def test_fab_1_should_be_1(self):
        result = fibo.recur_fibo(1)
        self.assertEqual(result,1)

    def test_fab_2_shoulde_1(self):
        result = fibo.recur_fibo(2)
        self.assertEqual(result,1)

    def test_fab_3_shoulde_2(self):
        result = fibo.recur_fibo(3)
        self.assertEqual(result,2)

    def test_fab_4_shoulde_3(self):
        result = fibo.recur_fibo(4)
        self.assertEqual(result,3)
    
    def test_fab_5_shoulde_5(self):
        result = fibo.recur_fibo(5)
        self.assertEqual(result,5)
    
    def test_fab_6_shoulde_8(self):
        result = fibo.recur_fibo(5)
        self.assertEqual(result,5)

    def test_fab_13_shoulde_233(self):
        result = fibo.recur_fibo(13)
        self.assertEqual(result,233)
    
    def test_fab_20_shoulde_6765(self):
        result = fibo.recur_fibo(20)
        self.assertEqual(result,6765)
    