import unittest
import unittest.mock as Mock
import view_regist 

class testViewRegist(unittest.TestCase):

    # how to run code
    # use command : python -m unittest test_view_regist.py -v

    @Mock.patch('view_regist.dir', return_value='__name__')
    def test_blueprint___name__(self, mock):
        mock.get_subblueprints.return_value = '__name__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__name__')

    @Mock.patch('view_regist.dir', return_value='__package__')
    def test_blueprint___package__(self, mock):
        mock.get_subblueprints.return_value = '__package__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__package__')

    @Mock.patch('view_regist.dir', return_value='__add__')
    def test_blueprint___add__(self, mock):
        mock.get_subblueprints.return_value = '__add__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__add__')

    @Mock.patch('view_regist.dir', return_value='__class__')
    def test_blueprint___class__(self, mock):
        mock.get_subblueprints.return_value = '__class__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__class__')

    @Mock.patch('view_regist.dir', return_value='__class_getitem__')
    def test_blueprint___class_getitem__(self, mock):
        mock.get_subblueprints.return_value = '__class_getitem__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__class_getitem__')

    @Mock.patch('view_regist.dir', return_value='__contains__')
    def test_blueprint___contains__(self, mock):
        mock.get_subblueprints.return_value = '__contains__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__contains__')

    @Mock.patch('view_regist.dir', return_value='__delattr__')
    def test_blueprint___delattr__(self, mock):
        mock.get_subblueprints.return_value = '__delattr__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__delattr__')
  
    @Mock.patch('view_regist.dir', return_value='__delitem__')
    def test_blueprint___delitem__(self, mock):
        mock.get_subblueprints.return_value = '__delitem__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__delitem__')

    @Mock.patch('view_regist.dir', return_value='__dir__')
    def test_blueprint___dir__(self, mock):
        mock.get_subblueprints.return_value = '__dir__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__dir__')

    @Mock.patch('view_regist.dir', return_value='__doc__')
    def test_blueprint___doc__(self, mock):
        mock.get_subblueprints.return_value = '__doc__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__doc__')

    @Mock.patch('view_regist.dir', return_value='__eq__')
    def test_blueprint___eq__(self, mock):
        mock.get_subblueprints.return_value = '__eq__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__eq__')

    @Mock.patch('view_regist.dir', return_value='__format__')
    def test_blueprint___format__(self, mock):
        mock.get_subblueprints.return_value = '__format__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__format__')

    @Mock.patch('view_regist.dir', return_value='__ge__')
    def test_blueprint___ge__(self, mock):
        mock.get_subblueprints.return_value = '__ge__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__ge__')

    @Mock.patch('view_regist.dir', return_value='__getattribute__')
    def test_blueprint___getattribute__(self, mock):
        mock.get_subblueprints.return_value = '__getattribute__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__getattribute__')

    @Mock.patch('view_regist.dir', return_value='__getitem__')
    def test_blueprint___getitem__(self, mock):
        mock.get_subblueprints.return_value = '__getitem__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__getitem__')

    @Mock.patch('view_regist.dir', return_value='__gt__')
    def test_blueprint___gt__(self, mock):
        mock.get_subblueprints.return_value = '__gt__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__gt__')

    @Mock.patch('view_regist.dir', return_value='__hash__')
    def test_blueprint___hash__(self, mock):
        mock.get_subblueprints.return_value = '__hash__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__hash__')

    @Mock.patch('view_regist.dir', return_value='__iadd__')
    def test_blueprint___iadd__(self, mock):
        mock.get_subblueprints.return_value = '__iadd__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__iadd__')

    @Mock.patch('view_regist.dir', return_value='__imul__')
    def test_blueprint___imul__(self, mock):
        mock.get_subblueprints.return_value = '__imul__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__imul__')

    @Mock.patch('view_regist.dir', return_value='__init__')
    def test_blueprint___init__(self, mock):
        mock.get_subblueprints.return_value = '__init__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__init__')

    @Mock.patch('view_regist.dir', return_value='__init_subclass__')
    def test_blueprint___init_subclass__(self, mock):
        mock.get_subblueprints.return_value = '__init_subclass__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__init_subclass__')

    @Mock.patch('view_regist.dir', return_value='__iter__')
    def test_blueprint___iter__(self, mock):
        mock.get_subblueprints.return_value = '__iter__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__iter__')

    @Mock.patch('view_regist.dir', return_value='__le__')
    def test_blueprint___le__(self, mock):
        mock.get_subblueprints.return_value = '__le__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__le__')

    @Mock.patch('view_regist.dir', return_value='__len__')
    def test_blueprint___len__(self, mock):
        mock.get_subblueprints.return_value = '__len__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__len__')

    @Mock.patch('view_regist.dir', return_value='__lt__')
    def test_blueprint___lt__(self, mock):
        mock.get_subblueprints.return_value = '__lt__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__lt__')

    @Mock.patch('view_regist.dir', return_value='__mul__')
    def test_blueprint___mul__(self, mock):
        mock.get_subblueprints.return_value = '__mul__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__mul__')

    @Mock.patch('view_regist.dir', return_value='__ne__')
    def test_blueprint___ne__(self, mock):
        mock.get_subblueprints.return_value = '__ne__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__ne__')

    @Mock.patch('view_regist.dir', return_value='__new__')
    def test_blueprint___new__(self, mock):
        mock.get_subblueprints.return_value = '__new__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__new__')

    @Mock.patch('view_regist.dir', return_value='__reduce__')
    def test_blueprint___reduce__(self, mock):
        mock.get_subblueprints.return_value = '__reduce__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__reduce__')

    @Mock.patch('view_regist.dir', return_value='__reduce_ex__')
    def test_blueprint___reduce_ex__(self, mock):
        mock.get_subblueprints.return_value = '__reduce_ex__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__reduce_ex__')

    @Mock.patch('view_regist.dir', return_value='__repr__')
    def test_blueprint___repr__(self, mock):
        mock.get_subblueprints.return_value = '__repr__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__repr__')

    @Mock.patch('view_regist.dir', return_value='__reversed__')
    def test_blueprint___reversed__(self, mock):
        mock.get_subblueprints.return_value = '__reversed__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__reversed__')

    @Mock.patch('view_regist.dir', return_value='__rmul__')
    def test_blueprint___rmul__(self, mock):
        mock.get_subblueprints.return_value = '__rmul__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__rmul__')

    @Mock.patch('view_regist.dir', return_value='__setattr__')
    def test_blueprint___setattr__(self, mock):
        mock.get_subblueprints.return_value = '__setattr__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__setattr__')

    @Mock.patch('view_regist.dir', return_value='__setitem__')
    def test_blueprint___setitem__(self, mock):
        mock.get_subblueprints.return_value = '__setitem__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__setitem__')

    @Mock.patch('view_regist.dir', return_value='__sizeof__')
    def test_blueprint___sizeof__(self, mock):
        mock.get_subblueprints.return_value = '__sizeof__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__sizeof__')

    @Mock.patch('view_regist.dir', return_value='__str__')
    def test_blueprint___str__(self, mock):
        mock.get_subblueprints.return_value = '__str__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__str__')

    @Mock.patch('view_regist.dir', return_value='__subclasshook__')
    def test_blueprint___subclasshook__(self, mock):
        mock.get_subblueprints.return_value = '__subclasshook__'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'__subclasshook__')

    @Mock.patch('view_regist.dir', return_value='append')
    def test_blueprint_append(self, mock):
        mock.get_subblueprints.return_value = 'append'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'append')

    @Mock.patch('view_regist.dir', return_value='clear')
    def test_blueprint_clear(self, mock):
        mock.get_subblueprints.return_value = 'clear'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'clear')

    @Mock.patch('view_regist.dir', return_value='copy')
    def test_blueprint_copy(self, mock):
        mock.get_subblueprints.return_value = 'copy'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'copy')

    @Mock.patch('view_regist.dir', return_value='count')
    def test_blueprint_count(self, mock):
        mock.get_subblueprints.return_value = 'count'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'count')

    @Mock.patch('view_regist.dir', return_value='extend')
    def test_blueprint_extend(self, mock):
        mock.get_subblueprints.return_value = 'extend'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'extend')

    @Mock.patch('view_regist.dir', return_value='index')
    def test_blueprint_index(self, mock):
        mock.get_subblueprints.return_value = 'index'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'index')

    @Mock.patch('view_regist.dir', return_value='insert')
    def test_blueprint_insert(self, mock):
        mock.get_subblueprints.return_value = 'insert'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'insert')

    @Mock.patch('view_regist.dir', return_value='pop')
    def test_blueprint_pop(self, mock):
        mock.get_subblueprints.return_value = 'pop'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'pop')

    @Mock.patch('view_regist.dir', return_value='remove')
    def test_blueprint_remove(self, mock):
        mock.get_subblueprints.return_value = 'remove'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'remove')

    @Mock.patch('view_regist.dir', return_value='reverse')
    def test_blueprint_reverse(self, mock):
        mock.get_subblueprints.return_value = 'reverse'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'reverse')

    @Mock.patch('view_regist.dir', return_value='sort')
    def test_blueprint_sort(self, mock):
        mock.get_subblueprints.return_value = 'sort'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'sort')

    @Mock.patch('view_regist.dir', return_value='[]')
    def test_blueprint_null(self, mock):
        mock.get_subblueprints.return_value = '[]'
        blueprints = mock.get_subblueprints.return_value
        self.assertEqual(blueprints,'[]')

if __name__ == '__main__':
    unittest.main()
