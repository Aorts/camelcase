def get_subblueprints(views=[]):
    blueprints = []
    print(dir(views))
    for view in views:
        blueprints.append(view.module)
        if 'subviews' in dir(view):
            for module in get_subblueprints(view.subviews):
                if view.module.url_prefix and module.url_prefix:
                    module.url_prefix = view.module.url_prefix + \
                        module.url_prefix
                print(blueprints)
                return blueprints

if __name__ == '__main__':
    get_subblueprints()